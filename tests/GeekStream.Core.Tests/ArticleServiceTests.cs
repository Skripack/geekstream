using System;
using System.Collections.Generic;
using GeekStream.Core.Services;
using GeekStream.Core.Entities;
using GeekStream.Infrastructure;
using Moq;
using Xunit;
using GeekStream.Core.Data;
using GeekStream.Core.Services.ArticleService;

namespace GeekStream.Core.Tests
{
    public class ArticleServiceTests
    {
        [Fact]
        public void GetAllNeedToReturnAll()
        {
            var repositoryMock = new Mock<IArticleRepository>();            
            var result = new List<Article> {
                new Article
                {
                    Id = "1",
                    CreatedAt = new DateTime(2021, 1, 1),
                    Author = new User
                    {
                        FirstName = "Andrew",
                        LastName = "Pomorew",
                        Email = "andrew@mail.ru"
                    },
                    Keywords = new List<string>
                            {
                                "keyword1",
                                "keyword2",
                                "keyword3"
                            },
                    Rating = 0,
                    Status = ArticleStatus.Draft
                },
                new Article
                {
                    Id = "2",
                    CreatedAt = new DateTime(2021, 1, 2),
                    Author = new User
                    {
                        FirstName = "Ivan",
                        LastName = "Groza",
                        Email = "ivan@mail.ru"
                    },
                    Keywords = new List<string>
                    {
                                    "keyword4",
                                    "keyword5",
                                    "keyword6"
                    },
                    Rating = 0,
                    Status = ArticleStatus.Published
                },
                new Article
                {
                    Id = "3",
                    CreatedAt = new DateTime(2021, 1, 2),
                    Author = new User
                    {
                        FirstName = "Sam",
                        LastName = "Hari",
                        Email = "sam@mail.ru"
                    },
                    Keywords = new List<string>
                    {
                                    "keyword5",
                                    "keyword6",
                                    "keyword7"
                    },
                    Rating = 0,
                    Status = ArticleStatus.Published
                }
            };
            repositoryMock.Setup(x => x.GetAll()).Returns(result);

            var service = new ArticleService_v1(repositoryMock.Object);

            Assert.Equal(result, service.GetArticles());
        }        
    }
}
