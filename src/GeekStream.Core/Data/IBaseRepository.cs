﻿using System;
using System.Collections.Generic;
using GeekStream.Core.Entities;

namespace GeekStream.Core.Data
{
    public interface IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        public void Add(TEntity entity);
        public void Remove(TEntity entity);
        public TEntity GetById(string id);
        public List<TEntity> GetAll();
        public List<TEntity> GetWhere(Func<TEntity, bool> condition);
    }
}
