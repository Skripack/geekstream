﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.Entities;

namespace GeekStream.Core.Data
{
    public interface IArticleRepository : IBaseRepository<Article>
    {

    }
}
