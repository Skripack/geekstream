﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Entities
{
    public class Reviewer
    {
        public User Rev { get; set; }
        public bool isAccepted { get; set; }
    }
}
