﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Entities
{
    public class Comment : BaseEntity
    {
        User Author { get; set; }
        string Message { get; set; }
        int Rating { get; set; }
        List<Image> Images { get; set; }
    }
}
