﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Entities
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Mark> CommentMarks { get; set; }
        public List<Mark> ArticleMarks { get; set; }
        public Image Avatar { get; set; }
        public List<Category> CategorySubscription { get; set; }
        public List<User> UserSubscription { get; set; }
    }
}
