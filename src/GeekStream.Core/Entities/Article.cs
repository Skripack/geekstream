﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Entities
{
    public class Article : BaseEntity
    {
        public User Author { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public List<string> Keywords { get; set; }
        public List<Message> ReviewComments { get; set; }
        public List<Reviewer> Reviewers { get; set; }
        public List<Comment> Comments { get; set; }
        public Category Cat { get; set; }
        public List<Image> Images { get; set; }
        public int Rating { get; set; }
        public ArticleStatus Status { get; set; }

    }
}
