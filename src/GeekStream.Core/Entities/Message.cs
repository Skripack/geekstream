﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Entities
{
    public class Message : BaseEntity
    {
        User From { get; set; }
        User To { get; set; }
        string Content { get; set; }
        List<Image> Images { get; set; }
    }
}
