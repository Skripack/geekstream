﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Entities
{
    public class Category : BaseEntity
    {
        public Image SmallIcon { get; set; }
        public Image LargeIcon { get; set; }
        public string Description { get; set; }
        public List<User> Moderators { get; set; }
    }
}
