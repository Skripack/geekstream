﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Core.Entities
{
    public class Image : BaseEntity
    {
        public string ImageUrl { get; set; }
    }
}
