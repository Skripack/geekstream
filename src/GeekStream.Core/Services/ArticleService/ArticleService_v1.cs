﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.Services;
using GeekStream.Core.Entities;
using GeekStream.Core.Data;

namespace GeekStream.Core.Services.ArticleService
{
    public class ArticleService_v1 : IArticleService
    {
        private readonly IArticleRepository _articleRepository;

        public ArticleService_v1(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public List<Article> GetArticles()
        {
            return _articleRepository.GetAll();
        }
        public List<Article> GetOwnArticles(User author)
        {
            return _articleRepository.GetWhere(article => article.Author == author);
        }
    }
}
