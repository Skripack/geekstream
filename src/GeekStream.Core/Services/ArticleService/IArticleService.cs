﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.Entities;

namespace GeekStream.Core.Services.ArticleService
{
    public interface IArticleService
    {
        public List<Article> GetArticles();
        public List<Article> GetOwnArticles(User author);
    }
}
