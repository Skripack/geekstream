﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeekStream.Core.Data;
using GeekStream.Core.Entities;
using GeekStream.Infrastructure.DataAccess;

namespace GeekStream.Infrastructure
{
    public class ArticleRepository : BaseRepository<Article>, IArticleRepository
    {
        public ArticleRepository() : base()
        {
        }
    }
}
