﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Core.Data;
using GeekStream.Core.Entities;

namespace GeekStream.Infrastructure.DataAccess
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity
    {
        private List<TEntity> _entities;

        public BaseRepository()
        {
            _entities = new List<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _entities.Add(entity);
        }
        public void Remove(TEntity entity)
        {
            _entities.Remove(entity);
        }
        public TEntity GetById(string id)
        {
            return _entities
                .Select(x => x)
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }
        public List<TEntity> GetAll()
        {
            return _entities;
        }
        public List<TEntity> GetWhere(Func<TEntity, bool> condition)
        {
            return _entities
                .Select(x=>x)
                .Where(condition)
                .ToList<TEntity>();
        }
    }
}
